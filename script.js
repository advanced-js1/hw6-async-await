async function getIp() {
	let response = await fetch("https://api.ipify.org/?format=json");
	let data = await response.json();
	let { ip } = data;
	return ip;
}

async function getInfo() {
	const ip = await getIp();
	console.log(ip);
	let response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city`);
	let data = await response.json();
	
	const { continent, country, regionName, city } = data;

	const infoELement = document.querySelector('.info-container');
	infoELement.innerHTML = `				
		<p>Континент: ${continent}</p>
		<p>Країна: ${country}</p>
		<p>Регіон: ${regionName}</p>
		<p>Місто: ${city}</p>
	`;
}


const btn = document.querySelector('.btn');

btn.addEventListener('click', () => {
	getInfo();	
})